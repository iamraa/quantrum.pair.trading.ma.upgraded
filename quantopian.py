"""
Парный трейдинг. Дневной таймфрейм.

# 2016: EQY, T; ('DIA', 'SLB'); ('E', 'MMP');
# 2015: ('DRE', 'O'); ('CIT', 'STT'); ('ALNY', 'DATA'); ('DVA', 'GEL'); ('AWK', 'DAL'); PNR, FDX; ('AOS', 'CRM'); ('ASML', 'DOV'); ('BHI', 'HLT'); ('MMP', 'PH')
# HalfYear: ('CF', 'CMI'); ('CRM', 'CVG');
# 2014-2015: ('DRE', 'O'); H, MMP; ('CMS', 'SPG');

# 2014: ('SMH', 'SSO'), ('AEP', 'AKR'), ('AIG', 'BUD'), ('BID', 'CA'), ('CAH', 'CHKP')

r2 example:
('VAL', 'TLK')
('CF', 'CMI')
('ALNY', 'DATA')

"""
import talib
import pandas as pd
import numpy as np
import statsmodels.api as sm
from statsmodels.tsa.stattools import adfuller


# подготовка теста
def initialize(context):
    # пара
    context.pair = tuple(
        symbols('DIS', 'MPC'),
    )
    context.opened = dict()
    context.opened[context.pair] = 'closed'

    context.coint_check_lookback = 130  # 120 - default

    # trades filter
    context.filter_by_coint = True
    context.filter_by_r2 = True
    context.r2_min, context.r2_max = 0.5, 0.95

    # use get_performance() or get_log_performance()
    context.FUNC_PERFORMANCE = get_log_performance

    schedule_function(trade_zscore, date_rules.every_day(), time_rules.market_open(hours=1))

# ежедневная проверка сигналов
def trade_zscore(context, data):
    stocks = context.pair

    can_trade = data.can_trade(stocks)

    # выходим, если не можем торговать одной из акций
    for stock in stocks:
        if not can_trade[stock]:
            log.warn("Can't trade " + stock.symbol)
            return

    # загрузка исторических данных
    hist = data.history(stocks, ['open', 'high', 'low', 'close'], 500, '1d')

    # переводим в относительные значения
    check_length = context.coint_check_lookback
    performance = [
        context.FUNC_PERFORMANCE(hist['close'][stocks[0]][-check_length:].as_matrix()),
        context.FUNC_PERFORMANCE(hist['close'][stocks[1]][-check_length:].as_matrix())
    ]

    spread_performance = pd.Series(performance[0] - performance[1])
    spread_price = hist['close'][stocks[0]][-check_length:] - hist['close'][stocks[1]][-check_length:]

    spread = spread_price
    spread = spread_performance
    zscore = zscore_std(spread)  # standard
    #zscore = z_score_ma(spread, short=5, long=50)  # by MA cross

    # сигнал
    z = zscore.iloc[-1]

    if len(spread[-check_length:].dropna()) < check_length:
        log.warn("Can't trade with short length")
        return

    # проверка стационарности
    result = adfuller(spread.iloc[-check_length:])
    score, pvalue, crit = result[0], result[1], result[4]
    coint = score < crit['5%']

    close_positions = False  # condition to stop trades
    # check cointegration
    if context.filter_by_coint:
        if not coint:
            close_positions = True

    # get R2 from OLS
    X = performance[0][-context.coint_check_lookback:].copy()
    Y = performance[1][-context.coint_check_lookback:].copy()
    r2 = get_r2(Y, X)

    # check r2
    if context.filter_by_r2:
        if not (context.r2_min < r2 <= context.r2_max):
            close_positions = True

    if close_positions:
        # если пропала стационарность, закрываемся
        order_target_percent(stocks[0], 0)
        order_target_percent(stocks[1], 0)
        context.opened[stocks] = 'closed'
    elif z > 1 and context.opened[stocks] == 'closed':
        # если опережает акция А
        order_target_percent(stocks[0], -1)
        order_target_percent(stocks[1], 1)
        context.opened[stocks] = 'short'
    elif z < -1 and context.opened[stocks] == 'closed':
        # если опережает акция Б
        order_target_percent(stocks[0], 1)
        order_target_percent(stocks[1], -1)
        context.opened[stocks] = 'long'
    elif (context.opened[stocks] == 'long' and z > 0) or (context.opened[stocks] == 'short' and z < 0):
        # рядом с нулем закрываем позицию
        order_target_percent(stocks[0], 0)
        order_target_percent(stocks[1], 0)
        context.opened[stocks] = 'closed'

    # собираем историю значений
    sign = abs(z) / z if z else 0
    value0 = context.portfolio.positions[stocks[0]].amount * context.portfolio.positions[stocks[0]].last_sale_price
    value1 = context.portfolio.positions[stocks[1]].amount * context.portfolio.positions[stocks[1]].last_sale_price
    record(**{
            'zscore': z if abs(z) <= 2.05 else sign*2.05,
            #'z-perf': zscore_std(spread_performance).iloc[-1],
            #'z-price': zscore_std(spread_price).iloc[-1],
            'coint': int(coint),
            'r2': r2,
            stocks[0].symbol: value0,
            stocks[1].symbol: value1
          })

def zscore_std(series):
    return (series - series.mean()) / np.std(series)

def z_score_ma(spread, short=10, long=60):
    # Получаем разницу цен двух наборов данных
    difference = spread
    difference.name = 'diff'

    # Получаем 10-дневную скользящую среднюю от разницы
    diff_mavg_short = difference.rolling(window=short, center=False).mean()
    diff_mavg_short.name = 'diff {0}d mavg'.format(short)

    # Получаем 60-дневную скользящую среднюю от разницы
    diff_mavg_long = difference.rolling(window=long, center=False).mean()
    diff_mavg_long.name = 'diff {0}d mavg'.format(long)

    # Получаем скользящее 60-дневное стандартное отклонение
    std_long = pd.rolling_std(difference, window=long)
    std_long.name = 'std {0}d'.format(long)

    # Рассчитываем z-оценку на каждый день
    zscore = (diff_mavg_short - diff_mavg_long)/std_long
    zscore.name = 'z-ma {0}d/{1}d'.format(short, long)

    return zscore


def get_performance(a):
    """
    Convert vector to performance

    a: numpy.ndarray with prices
    """
    a = a.round(2)
    return np.insert(np.cumsum(np.diff(a) / a[:-1] * 100.), 0, 0)


def get_log_performance(a):
    """
    Convert vector to log performance

    a: numpy.ndarray with prices
    """
    a = a.round(2)
    return np.insert(np.diff(np.log(a)).cumsum(), 0, 0) * 100


def get_diff(y):
    # Метод наименьших квадратов
    x = np.array([i for i in range(len(y))])
    arr = np.vstack([x, np.ones(len(x))]).T
    m, c = np.linalg.lstsq(arr, y)[0]
    #print(m, c)
    return c

def get_r2(Y, X):
    X = sm.add_constant(X)
    model = sm.OLS(Y, X).fit()
    return model.rsquared