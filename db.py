import psycopg2
import psycopg2.extras
from datetime import datetime, date, timedelta
from pandas_datareader import data, wb
import numpy as np

class Db(object):
    _connection = None

    def __init__(self, host="localhost", user="user", password="1", db="db"):
        try:
            self._connection = psycopg2.connect("host='{0}' user='{1}' password='{2}' dbname='{3}'".format(
                    host, user, password, db))
        except psycopg2.Error as err:
            print("Connection error: {}".format(err))
            self._connection.close()

    def query(self, sql, params=None, cursor='list'):
        if not self._connection:
            return False

        data = False

        if cursor == 'dict':
            # Assoc cursor
            factory = psycopg2.extras.DictCursor
        else:
            # Standard cursor
            factory = psycopg2.extensions.cursor
        try:
            cur = self._connection.cursor(cursor_factory=factory) # by column name
            cur.execute(sql, params)
            data = cur.fetchall()
        except psycopg2.Error as err:
            print("Query error: {}".format(err))

        return data

db = Db(host="localhost", user="developer", password="1", db="go_finance")

def get_prices_db(symbols, dt_from=date.today(), period=90, is_adj=True):
    """
    Get prices from database for one or multiple symbols.
    """
    table = 'prices'

    dt_to = dt_from - timedelta(days=period)
    cond = {
        'symbols': symbols,
        'to': dt_from,
        'from': dt_to
    }

    # FIXME Change to view
    sql = """
        SELECT
            string_agg(symbol, ',') AS symbol_list
            , string_agg(dt::text, ',') AS dt_list
            , string_agg(open::text, ',') AS open_list
            , string_agg(high::text, ',') AS high_list
            , string_agg(low::text, ',') AS low_list
            , string_agg("close"::text, ',') AS close_list
            , string_agg(volume::text, ',') AS volume_list
            , string_agg(adj::text, ',') AS adj_list
        FROM v_{0}_fast
        WHERE symbol IN (SELECT unnest(%(symbols)s)) AND dt BETWEEN %(from)s AND %(to)s
        """.format(table)
    
    r = db.query(sql, cond)[0]
    d = {
        'symbol': np.array(r[0].split(',')),
        'dt': np.array(r[1].split(','), dtype='datetime64'),
        'open': np.fromstring(r[2], sep=',') / 10000,
        'high': np.fromstring(r[3], sep=',') / 10000,
        'low': np.fromstring(r[4], sep=',') / 10000,
        'close': np.fromstring(r[5], sep=',') / 10000,
        'volume': np.fromstring(r[6], sep=','),        
    }
    
    if is_adj:
        adj = np.fromstring(r[7], sep=',') / 10000
        adj_exists = adj > 0
        ratio = adj[adj_exists] / d['close'][adj_exists]

        d['open'][adj_exists] = d['open'][adj_exists] * ratio
        d['high'][adj_exists] = d['high'][adj_exists] * ratio
        d['low'][adj_exists] = d['low'][adj_exists] * ratio
        d['close'][adj_exists] = d['close'][adj_exists] * ratio

    return d


def get_performance(a):
    """
    Convert vector to performance
    
    a: numpy.ndarray with prices
    """
    a = a.round(2)
    return np.insert(np.cumsum(np.diff(a) / a[:-1] * 100.), 0, 0)


def get_log_performance(a):
    """
    Convert vector to log performance
    
    a: numpy.ndarray with prices
    """
    a = a.round(2)
    return np.insert(np.diff(np.log(a)).cumsum(), 0, 0) * 100



def prepare_pair(S1, S2, to_performance=True, is_log=False):
    """
    Cut vectors to the minimum length
    
    S1: numpy.ndarray with prices
    S2: numpy.ndarray with prices
    to_performance: convert to performance 
    is_log: use log returns for performance
    """
    # проверяем длину истории и обрезаем до минимальной
    if len(S1) < len(S2):
        S2 = S2[-len(S1):]
    elif len(S2) < len(S1):
        S1 = S1[-len(S2):]

    # конвертируем в относительные величины
    if to_performance:
        func = get_performance if not is_log else get_log_performance
            
        S1 = func(S1)
        S2 = func(S2)

    return S1, S2

def get_prices_remote(symbols, dt_end=date.today(), source='yahoo'):
    stocks = symbols
    if source == 'yahoo':
        ls_key = 'Adj Close'
    elif source == 'google':
        ls_key = 'Close'
    start = dt_end - timedelta(days=700)
    end = dt_end
    f = data.DataReader(stocks, source, start, end)
    return f[ls_key]

def months_generator(x):
    iterator = iter(x)
    prev_item = None
    for item in iterator:
        if prev_item is None or prev_item.month != item.month:
            yield item
        prev_item = item