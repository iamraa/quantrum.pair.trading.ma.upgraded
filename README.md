# README #

Статьи:

* [Бэктестинг: улучшаем поиск для парного трейдинга](https://quantrum.me/1009-backtesting-uluchshaem-poisk-dlya-parnogo-trejdinga/)
* [Парный трейдинг: фильтруем пары по смешанной корреляции](https://quantrum.me/1391-parnyj-trejding-filtruem-pary-po-smeshannoj-korrelyacii/)


Алгоритм Quantopian размещен в скрипте: **quantopian.py**

## Database fields (PostgreSQL) ##

* **symbol**	character varying(6)
* **dt**	date
* **open**	bigint [0]
* **high**	bigint [0]
* **low**	bigint [0]
* **close**	bigint [0]
* **volume**	numeric(20,0) [0]
* **adj**	numeric(20,0) NULL

